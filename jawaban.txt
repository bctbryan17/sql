1. membuat database
create database myshop;

2. membuat table di dalam database
use myshop;
create table users(
    -> id integer auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );
create table categories(
    -> id integer auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
 create table items(
    -> id integer auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

3. memasukkan data pada table
 insert into users(name,email,password)VALUES("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jane123");
 insert into categories(name)VALUES("gadget"),("cloth"),("men"),("women"),("branded");
 insert into items(name,description,price,stock,category_id)VALUES("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. mengambil data dari database
select id,name,email from users;
select * from items where price > 1000000;
select * from items where name like "%uniklo%";
select i.name,i.description,i.price,i.stock,i.category_id, c.name  kategori from items i left join categories c  on i.category_id = c.id;

5. mengubah data dari database
UPDATE items SET price=2500000 WHERE name="Sumsang b50";